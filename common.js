'use strict'
let password = document.querySelector('.password');
let passwordConfirm = document.querySelector('.password-confirm');

const eye = document.querySelector('#eye');
const eyeConfirm = document.querySelector('#eye-confirm');

const error = document.querySelector('.error');
const submit = document.querySelector('.btn');

eye.addEventListener('click', () => {
    eyeClicker(eye, password);
})

eyeConfirm.addEventListener('click', () => {
    eyeClicker(eyeConfirm, passwordConfirm);
})

function eyeClicker(eyeIcon, passLine) {
    if (eyeIcon.classList.contains('fa-eye')) {
        eyeIcon.classList.replace('fa-eye', 'fa-eye-slash');
        passLine.type = 'text';
    } else {
        passLine.type = 'password';
        eyeIcon.classList.replace('fa-eye-slash', 'fa-eye');
    }
}

submit.addEventListener('click', (e) => {
    if (password.value === passwordConfirm.value && password.value !== '' && passwordConfirm !== '') {
        error.style.visibility = 'hidden';
        alert('You are welcome');
    } else error.style.visibility = 'visible';
    e.preventDefault();
})